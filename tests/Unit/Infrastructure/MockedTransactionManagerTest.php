<?php

declare(strict_types=1);

namespace FriendsOfDdd\TransactionManager\Tests\Unit\Infrastructure;

use FriendsOfDdd\TransactionManager\Infrastructure\MockedTransactionManager;
use LogicException;
use PHPUnit\Framework\TestCase;

class MockedTransactionManagerTest extends TestCase
{
    private MockedTransactionManager $transactionManager;

    protected function setUp(): void
    {
        $this->transactionManager = new MockedTransactionManager();
    }

    public function testInvokeCallbackWithoutExceptionsStillIncrementsTheValue(): void
    {
        $increment = 0;

        $this->transactionManager->wrapInTransaction(
            static function () use (&$increment) {
                ++$increment;
            }
        );

        self::assertTrue(true, 'No exception was thrown');
        self::assertSame(1, $increment);
    }

    public function testInvokeCallbackClassWithoutExceptionsStillIncrementsTheValue(): void
    {
        $increment = 0;
        $invocableObect = new class ($increment) {
            public function __construct(public int $increment)
            {
            }

            public function __invoke(): void
            {
                ++$this->increment;
            }
        };

        $this->transactionManager->wrapInTransaction($invocableObect);

        self::assertTrue(true, 'No exception was thrown');
        self::assertSame(1, $invocableObect->increment);
    }

    public function testInvokeCallbackThrowsExceptionBeforeIncrementingTheValue(): void
    {
        $increment = 0;
        $this->transactionManager->expectedException = new LogicException('SomethingHappened');

        $this->expectExceptionObject($this->transactionManager->expectedException);

        $this->transactionManager->wrapInTransaction(
            static function () use (&$increment) {
                ++$increment;
            }
        );

        self::assertSame(0, $increment);
    }
}
